<h1 align="center">Todo App</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://hariprakashtodoapp.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/hariprakash20/todo-app-2.0">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/hH6PbOHBdPm6otzw2De5">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [How To Use](#how-to-use)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

[![Screenshot-2023-03-21-at-3-52-42-PM.png](https://i.postimg.cc/JhfWm76Q/Screenshot-2023-03-21-at-3-52-42-PM.png)](https://postimg.cc/SXfPf4pX)

### Built With


## Features


## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/hariprakash20/todo-app-2.0.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Acknowledgements


## Contact

- Gitlab [@hariprakash20](https://gitlab.com/hariprakash20)
