let newId = 1;
const generateID = () => newId++;
const tasksContainer = document.getElementById("tasks-container")
const deleteAllBtn = document.getElementById("delete-all-btn")
const form = document.getElementById("to-do-form")

function addTask(task , tasks, completed){
    const taskObj = {
       id: generateID(),
       task: task,
       finish: false,
    }
    tasks.push(taskObj);
    printTasks(tasks ,completed)  
}

function printTasks(tasks, completed) {
    tasksContainer.textContent = ""
    tasks.forEach(taskObj => {
        let id = taskObj.id;
        let task = taskObj.task;
        let finish = taskObj.finish;
       let li = document.createElement("li")
       li.classList.add("task")
       let div = document.createElement("div")
       div.classList.add("task-checkbox")
       div.innerHTML =`
          <input class="task-input" type="checkbox" id="${id}">
          <label  class="task-label"  for="${id}">${task}</label>
       `
       li.appendChild(div)
       const i = document.createElement("I")
       i.classList.add("fa", "fa-trash", "delete-btn");
       completed ? i.classList.remove('hidden') : i.classList.add('hidden');
       div.firstElementChild.checked = finish;
       i.dataset.id = id;
       li.appendChild(i);
       tasksContainer.appendChild(li)
    })
    return false;
 }

function onlyCompleted(tasks, completed){
   completed = true;
   deleteAllBtn.classList.remove('hidden');
    form.classList.add('display-none');
    printTasks(tasks.filter(task => task.finish), completed)
    return false;
 }
function onlyActive(tasks, completed){
    deleteAllBtn.classList.add('hidden');
    form.classList.remove('display-none');
    printTasks(tasks.filter(task => !task.finish), completed)
    return false;
 }

module.exports = { addTask, printTasks , onlyActive ,onlyCompleted};