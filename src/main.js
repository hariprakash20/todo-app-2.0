const { addTask, printTasks , onlyActive ,onlyCompleted } = require('./func');

let tasks = []
let completed = false;
const tabsContainer = document.getElementById("to-do-tabs")
const tabs = document.querySelectorAll(".to-do-tab")
const form = document.getElementById("to-do-form")
const tasksContainer = document.getElementById("tasks-container")
const deleteAllBtn = document.getElementById("delete-all-btn")
const inputField = document.getElementById('input-field')
inputField.focus();

tabsContainer.addEventListener("click", event => {
       tabs.forEach(tab => tab.classList.remove("active-tab"));
       event.target.classList.add("active-tab")
       if(event.target.dataset.id == 1) {
          form.classList.remove('display-none');
          deleteAllBtn.classList.add('hidden');
          completed = printTasks(tasks, completed);
       }
       if(event.target.dataset.id == 2){
        completed = onlyActive(tasks , completed)
       } 
       if(event.target.dataset.id == 3){
        completed = onlyCompleted(tasks ,completed)
       }
       inputField.focus();
})

form.addEventListener("submit",event => {
    event.preventDefault()
    let task = form.task.value
    if(task.length > 0) addTask(task,tasks, completed);
    form.reset();
})

tasksContainer.addEventListener("click", event =>{
    tasks.map(task =>{
        if(task.id==event.target.id){
            task.finish = !task.finish;
        }
    })
    if(event.target.classList.contains("delete-btn")){
        tasks.map(task =>{
            if(task.id==event.target.dataset.id){
                tasks.splice(tasks.indexOf(task), 1);
            }
        })
        onlyCompleted(tasks, completed);
    }
})

deleteAllBtn.addEventListener("click", () =>{
    tasks = tasks.filter(task => { 
        return !task.finish 
    })
    completed = printTasks([]);
})